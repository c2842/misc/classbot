/*
Name:         Eoghan Conlon
Date Created: 29/6/2022
 */

import { SlashCommandBuilder, SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { CommandInteraction, Message, TextChannel } from "discord.js";
import { PG_Manager } from "../db";
import { Count, Highscore } from "../db_entities";

const command_set = new SlashCommandSubcommandBuilder().setName('set').setDescription('Start the count')
    .addNumberOption(option => option.setName('number').setDescription('Sets the starting number, if left blank default is 0').setRequired(false))
    .addNumberOption(option => option.setName('base').setDescription('Sets the number system to be used').setRequired(false));

const command_stop = new SlashCommandSubcommandBuilder().setName('stop').setDescription('Stops the count');

const command_reset = new SlashCommandSubcommandBuilder().setName('reset').setDescription('manually restarts the count');

const command = new SlashCommandBuilder().setName('count').setDescription('Counting bot replacement')
    .addSubcommand(command_set)
    .addSubcommand(command_stop)
    .addSubcommand(command_reset);

async function count_set(db: PG_Manager, interaction: CommandInteraction) {
    // Code lifted from admin.ts with a small change
    const channel = interaction.client.channels.cache.get(interaction.channelId) as TextChannel;
    const admin = channel.permissionsFor(interaction.user).has("ADMINISTRATOR", true);
    if (!admin) {
        return await interaction.reply('Only admin can start counts');
    }
    // End of borrowed code.
    // Functionality to be added at a later date. Const only to stop it yelling at me.
    const increment = 1;
    // assumes that there is no value here
    let initialvalue = 0;
    if (interaction.options.getNumber('number') !== null) {
        initialvalue = interaction.options.getNumber('number');
    }
    // assumes base 10 if no base is specified
    let base = 10;
     if (interaction.options.getNumber('base') != null) {
         base = interaction.options.getNumber('base');
     }
    await db.update_one(Count, {
        server: interaction.guildId,
        channel: interaction.channelId,
        user_set_up: interaction.user.id,
        initial_value: initialvalue,
        current_value: initialvalue,
        base: base,
        increment: increment,
        last_user: "",
    });
     const count_db = await db.get_items(Count, { server: interaction.guildId, channel: interaction.channelId });
     const count_info = count_db[0];
     await db.update_one(Highscore, {
         id: count_info.id,
         server: interaction.guildId,
         base: base,
         increment: increment,
         user: interaction.user.id,
         score: initialvalue,
     });
    return await interaction.reply({ content: `Counting started in this channel at base ${base}` });
}

async function count_reset(db: PG_Manager, interaction: CommandInteraction) {
    // Code lifted from admin.ts with a small change
    const channel = interaction.client.channels.cache.get(interaction.channelId) as TextChannel;
    const admin = channel.permissionsFor(interaction.user).has("ADMINISTRATOR", true);
    if (!admin) {
        return await interaction.reply('Only admin can reset counts');
    }
    // End of borrowed code.
    const is_count = await db.get_items(Count, { server: interaction.guildId, channel: interaction.channelId });
    if (is_count.length < 1) {
        return interaction.reply('There is no count initiated in this channel');
    }
    const count = is_count[0];
    await count_reset_core(db, count);
    return interaction.reply(`Count is reset to ${count.current_value} in base ${count.base}.`);
}

async function count_reset_core(db: PG_Manager, count: Count) {
    await db.delete(Count, { id: count.id });
    const new_count = await db.update_one(Count, {
        server: count.server,
        channel: count.channel,
        user_set_up: count.user_set_up,
        initial_value: count.initial_value,
        current_value: count.initial_value,
        base: count.base,
        last_user: '',
    });
    await db.update_one(Highscore, { base: count.base, id: new_count.id, score: 0, server: count.server, user: "" });
}

async function count_stop(db: PG_Manager, interaction: CommandInteraction) {
    const is_count = await db.get_items(Count, { server: interaction.guildId, channel: interaction.channelId });
    if (is_count.length < 1) {
        return await interaction.reply("You cannot stop counting in a channel that isn't set up for counting");
    }
    // Code lifted from admin.ts with a small change
    const channel = interaction.client.channels.cache.get(interaction.channelId) as TextChannel;
    const admin = channel.permissionsFor(interaction.user).has("ADMINISTRATOR", true);
    if (!admin) {
        return await interaction.reply('Only admin can stop counts');
    }
    // End of borrowed code.
    const count = is_count[0];
    const highscore_db = await db.get_items(Highscore, { server: interaction.guildId, id: count.id });
    const highscore_info = highscore_db[0];
    if (count.current_value > highscore_info.score) {
        highscore_info.user = count.last_user;
        highscore_info.score = count.current_value;
        await db.update_one(Highscore, highscore_info);
    }
    await db.delete(Count, { server: interaction.guildId, channel: interaction.channelId });
    return interaction.reply(`Counting has stopped in this channel. The final highscore of this session is ${highscore_info.score} held by <@${highscore_info.user}>`);
}

async function count_message(db: PG_Manager, message: Message) {
    const is_count = await db.get_items(Count, { server: message.guildId, channel: message.channelId });
    const count = is_count[0];
    if (count.last_user === message.author.id) {
        return;
    }
    const message_content = message.content;
    const number = parseInt(message_content, count.base);
    if (isNaN(number)) {
        return;
    }
    if (number === (count.current_value + 1)) {
        await db.update_one(Highscore, { id: count.id, score: number, user: message.author.id });
        count.current_value = number;
        count.last_user = message.author.id;
        await db.update_one(Count, count);
    } else {
        const highscore_db = await db.get_items(Highscore, { server: message.guildId, id: count.id });
        const highscore_info = highscore_db[0];
        await count_reset_core(db, count);
        return message.reply(`${message_content} is not correct, count reset to ${count.initial_value} in base ${count.base}. The high score was ${highscore_info.score} held by <@${highscore_info.user}>`);
    }
}

module.exports = {
    data: command,

    // its named execute to standardise it across modules
    async execute(db: PG_Manager, interaction: CommandInteraction) {
        // split out by the subcommand
        switch (interaction.options.getSubcommand()) {
            case "set" : {
                return await count_set(db, interaction);
            }
            case "stop" : {
                return await count_stop(db, interaction);
            }
            case "reset" : {
                return await count_reset(db, interaction);
            }
            default: {
                await interaction.reply('Please select a subcommand');
            }
        }
    },
    async message(db: PG_Manager, message: Message) {
        return await count_message(db, message);
    }
    ,
};