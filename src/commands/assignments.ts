/*
Creator:    Brendan Golden
Date:       2021-10-17
Description:This command is to manage the assignments channel
*/

import { SlashCommandBuilder, SlashCommandSubcommandBuilder } from '@discordjs/builders';
import {
  ButtonInteraction, CommandInteraction,
  MessageActionRow, MessageButton,
  MessageEmbedOptions,
  MessageReaction,
  PartialMessageReaction,
  PartialUser,
  TextChannel,
  User,
} from "discord.js";
import { PG_Manager } from "../db";
import { Assignments, AssignmentsReact, AssignmentsUser, Channels } from "../db_entities";
import { FindConditions, MoreThan } from "typeorm";

const command_new = new SlashCommandSubcommandBuilder().setName('new').setDescription('Set a new assignment')
    .addStringOption(option => option.setName('module').setDescription('module the assignment is for').setRequired(true))
    .addStringOption(option => option.setName('title').setDescription('name of teh assignment').setRequired(true))
    .addStringOption(option => option.setName('due').setDescription('when is it due').setRequired(true))
    .addStringOption(option => option.setName('link').setDescription('link to the sulis page').setRequired(true));

const command_edit = new SlashCommandSubcommandBuilder().setName('edit').setDescription('Edit a new assignment')
    .addNumberOption(option => option.setName('id').setDescription('ID of the assignment').setRequired(true))
    .addStringOption(option => option.setName('module').setDescription('module the assignment is for'))
    .addStringOption(option => option.setName('title').setDescription('name of teh assignment'))
    .addStringOption(option => option.setName('due').setDescription('when is it due'))
    .addStringOption(option => option.setName('link').setDescription('link to the sulis page'));

const command_list = new SlashCommandSubcommandBuilder().setName('list').setDescription('List Assignments')
    .addBooleanOption(option => option.setName('all').setDescription('Lists all assignments, past and present'))
    .addBooleanOption(option => option.setName('all_active').setDescription('Lists all active assignments'))
    .addStringOption(option => option.setName('module').setDescription('Filters assignments for a specific module'));

const command_delete = new SlashCommandSubcommandBuilder().setName('delete').setDescription('Edit a new assignment')
    .addNumberOption(option => option.setName('id').setDescription('ID of the assignment').setRequired(true));

const command_semester = new SlashCommandSubcommandBuilder().setName('semester').setDescription('Set a new assignment')
  .addStringOption(option => option.setName('message').setDescription('Message of the post'))
  .addStringOption(option => option.setName('module_1').setDescription('Module 1'))
  .addStringOption(option => option.setName('module_2').setDescription('Module 2'))
  .addStringOption(option => option.setName('module_3').setDescription('Module 3'))
  .addStringOption(option => option.setName('module_4').setDescription('Module 4'))
  .addStringOption(option => option.setName('module_5').setDescription('Module 5'));

const command = new SlashCommandBuilder().setName('assignment').setDescription('Replies with assignment!')
    .addSubcommand(command_new)
    .addSubcommand(command_list)
    .addSubcommand(command_edit)
    .addSubcommand(command_delete)
    .addSubcommand(command_semester);

// the emoji used for reactions
const react_emoji = '☕';

async function assignment_new(interaction: CommandInteraction, db: PG_Manager) {

  // needs to get the channel ID it posts out to
  const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

  if (channel_id.length == 0) {
    return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');
  }

  // get the channel we are sending it to
  const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

  // check the users permissions for teh channel
  const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
  if (!can_post) {
    return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);
  }

  // split out values
  const module = interaction.options.getString('module');
  const title = interaction.options.getString('title');
  const due = interaction.options.getString('due');
  const link = interaction.options.getString('link');

  // input validation
  if (!link.startsWith("https://")) {
    return await interaction.reply({ content: 'Link needs to start with https://', ephemeral: true });
  }

  const due_formatted = new Date(due);

  /*
      Gotta love chicken and egg stuff

      The embed output wants teh id of the assignment.
      The DB wants teh ID of the message

      The compromise is that its added tot eh db first
  */

  const result = await db.update_one(Assignments, {
    server: interaction.guildId,
    module: module,
    title: title,
    due: due_formatted.getTime(),
    link: link,
    user: interaction.user.id,
  });

  // create the embed
  const embed: MessageEmbedOptions = {
    title: `${module} - ${title}`,
    url: link,
    fields: [
      { name: 'Module', value: module, inline: true },
      { name: 'Name', value: title, inline: true },
      { name: 'Due', value: due_formatted.toLocaleString("en-IE"), inline: true },
      { name: 'Link', value: `[${title}](${link})`, inline: false },
      { name: 'Added by', value: `<@${interaction.user.id}>`, inline: true },
      { name: 'ID', value: result.id.toString(), inline: true },
    ],
  };

  // send it off
  const message = await channel.send({ embeds: [embed] });

  // add a reaction to track completion
  await message.react(react_emoji);

  // update the db with the message ID
  await db.update_one(Assignments, { id: result.id, message_id: message.id });

  // send a reply to the command user
  return await interaction.reply({ content: 'Assignment set', ephemeral: true });
}

async function assignment_edit(interaction: CommandInteraction, db: PG_Manager) {

  // needs to get the channel ID it posts out to
  const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

  if (channel_id.length == 0) {
    return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');
  }

  // get the channel we are sending it to
  const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

  // check the users permissions for teh channel
  const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
  if (!can_post) {
    return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);
  }

  // always requires teh ID
  const modified: Partial<Assignments> = {
    id: interaction.options.getNumber('id'),
    user: interaction.user.id,
  };

  const module = interaction.options.getString('module');
  if (module) {
    modified.module = module;
  }

  const title = interaction.options.getString('title');
  if (title) {
    modified.title = title;
  }

  const due = interaction.options.getString('due');
  if (due) {
    modified.due = new Date(due).getTime();
  }

  const link = interaction.options.getString('link');
  if (link) {
    if (!link.startsWith("https://")) {
      return await interaction.reply({ content: 'Link needs to start with https://', ephemeral: true });
    }
    modified.link = link;
  }

  // this takes the above modifications and merges it into one object, while saving teh changes
  const updated = await db.update_one_full(Assignments, modified);
  if (typeof updated === "undefined") {
    return await interaction.reply({ content: 'No existing item with that ID', ephemeral: true });
  }

  // update teh embed
  const embed: MessageEmbedOptions = {
    title: `${updated.module} - ${updated.title}`,
    url: updated.link,
    fields: [
      { name: 'Module', value: updated.module, inline: true },
      { name: 'Name', value: updated.title, inline: true },
      { name: 'Due', value: new Date(updated.due).toLocaleString("en-IE"), inline: true },
      { name: 'Link', value: `[${updated.title}](${updated.link})`, inline: false },
      { name: 'Added by', value: `<@${updated.user}>`, inline: true },
      { name: 'ID', value: updated.id.toString(), inline: true },
    ],
  };

  // find the original message
  const message = await channel.messages.fetch(updated.message_id);

  // send it off
  await message.edit({ embeds: [embed] });

  // send a reply to the command user
  return await interaction.reply({ content: `Assignment #${updated.id} updated`, ephemeral: true });
}

async function assignment_delete(interaction: CommandInteraction, db: PG_Manager) {

  // needs to get the channel ID it posts out to
  const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

  if (channel_id.length == 0) {
    return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');
  }

  // get the channel we are sending it to
  const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

  // check the users permissions for teh channel
  const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
  if (!can_post) {
    return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);
  }

  // split out values
  const id = interaction.options.getNumber('id');

  await db.delete(Assignments, { id: id, server: interaction.guildId });

  // send a reply to the command user
  return await interaction.reply({ content: `Assignment #${id} deleted`, ephemeral: true });
}

const toHMS = (secs) => {
  const sec_num = parseInt(secs, 10);
  const hours = Math.floor(sec_num / 3600);
  const minutes = Math.floor(sec_num / 60) % 60;
  const seconds = sec_num % 60;

  return [hours, minutes, seconds]
    .map(v => v < 10 ? "0" + v : v)
    .filter((v, i) => v !== "00" || i > 0)
    .join(":");
};

async function assignment_list(interaction: CommandInteraction, db: PG_Manager) {

  // this command can take more than 3s to complete, so deffer it
  await interaction.deferReply({ ephemeral: true });

  // gonan need to get the channel that assignments are posted in
  const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);
  if (channel_id.length == 0) {
    return await interaction.editReply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');
  }

  // see if #all has been pinged
  const all = interaction.options.getBoolean('all');
  const all_active = interaction.options.getBoolean('all_active');
  const module_choice = interaction.options.getString('module');

  // construct teh query
  const query: FindConditions<Assignments> = {
    server: interaction.guildId,
  };

  if (module_choice) {
    query.module = module_choice;
  }
  if (!all) {
    query.due = MoreThan(new Date().getTime());
  }

  // now search for it
  const assignments = await db.get_items(Assignments, query);

  if (assignments.length === 0) {
    return await interaction.editReply({ content: "No assignments match search" });
  }

  const reacts = {};
  for (const react of await db.get_items(AssignmentsReact, { user: interaction.user.id })) {
    if (react.complete) {
      reacts[react.assignment] = true;
    }
  }
  const subscribed = await db.get_items(AssignmentsUser, { server: interaction.guildId, user: interaction.user.id }).then(result => array_to_object(result, "module"));

  // newest first
  assignments.sort((a, b) => {
    return new Date(a.due).getTime() - new Date(b.due).getTime();
  }).reverse();

  const embeds = [];

  // only get thsi value once
  const now_epoch = new Date().getTime();
  // only 10 embeds are allowed
  for (let i = 0; i < assignments.length; i++) {
    const { module, title, link, due, user, id, message_id } = assignments[i];

    // default state
    if (!module_choice && !all && !all_active) {
      // only shows if the user is subscribed to a module
      if (!subscribed[module]) {
        continue;
      }

      // if neither of teh two options above are dione
      if (reacts[id]) {
        continue;
      }
    }

    let remaining = "00:00:00";
    if (due > now_epoch) {
      remaining = toHMS((due - now_epoch) / 1000);
    }

    if (all_active) {
      if (remaining === "00:00:00") {
        continue;
      }
    }

    const embed = {
      title: `${module} - ${title}`,
      url: link,
      fields: [
        { name: 'Module', value: module, inline: true },
        { name: 'Name', value: title, inline: true },
        { name: 'Due', value: new Date(due).toLocaleString("en-IE"), inline: true },
        { name: 'Time Remaining (hh:mm:ss)', value: remaining, inline: false },
        { name: 'Link', value: `[${title}](${link})`, inline: false },
        { name: 'Added by', value: `<@${user}>`, inline: true },
        { name: 'ID', value: id.toString(), inline: true },
        {
          name: 'Original message',
          value: `[Was in <#${channel_id[0].channel}>](https://discord.com/channels/${interaction.guildId}/${channel_id[0].channel}/${message_id})`,
          inline: true,
        },
      ],
    };
    embeds.push(embed);
  }

  // only 10 embeds can eb shown in a message
  while (embeds.length > 10) {
    embeds.shift();
  }

  if (embeds.length === 0) {
    return await interaction.editReply({ content: "No assignments match search, are you subscribed to them?" });
  }

  // send a reply to the command user
  return await interaction.editReply({ embeds: embeds });
}

async function assignment_semester(interaction: CommandInteraction, db: PG_Manager) {

  // needs to get the channel ID it posts out to
  const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

  if (channel_id.length == 0) {
    return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');
  }

  // get the channel we are sending it to
  const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

  // check the users permissions for teh channel
  const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
  if (!can_post) {
    return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);
  }

  const message = interaction.options.getString('message');
  const module_1 = interaction.options.getString('module_1');
  const module_2 = interaction.options.getString('module_2');
  const module_3 = interaction.options.getString('module_3');
  const module_4 = interaction.options.getString('module_4');
  const module_5 = interaction.options.getString('module_5');

  // if no modules are submitted then return
  if (!(module_1 || module_2 || module_3 || module_4 || module_5)) {
    return await interaction.reply(`No Module supplied`);
  }

  const row = new MessageActionRow();

  if (module_1) {
    row.addComponents(new MessageButton().setCustomId(`assignment_semester_${module_1}`).setLabel(module_1).setStyle('PRIMARY'),);
  }
  if (module_2) {
    row.addComponents(new MessageButton().setCustomId(`assignment_semester_${module_2}`).setLabel(module_2).setStyle('PRIMARY'),);
  }
  if (module_3) {
    row.addComponents(new MessageButton().setCustomId(`assignment_semester_${module_3}`).setLabel(module_3).setStyle('PRIMARY'),);
  }
  if (module_4) {
    row.addComponents(new MessageButton().setCustomId(`assignment_semester_${module_4}`).setLabel(module_4).setStyle('PRIMARY'),);
  }
  if (module_5) {
    row.addComponents(new MessageButton().setCustomId(`assignment_semester_${module_5}`).setLabel(module_5).setStyle('PRIMARY'),);
  }

  await interaction.reply({ content: message, components: [row] });
}

async function assignment_semester_module(interaction: ButtonInteraction, db: PG_Manager, module: string) {
  // this sets, unsets the users module

  // get from AssignmentsUser
  const is_set = await db.get_items(AssignmentsUser, { server: interaction.guildId, user: interaction.user.id, module: module });

  if (is_set.length === 0) {
    // not set, insert it now
    await db.update_one(AssignmentsUser, { server: interaction.guildId, user: interaction.user.id, module: module });
    await interaction.reply({ ephemeral: true, content: `Started following ${module}` });
  } else {
    // it is set, now unset
    await db.delete(AssignmentsUser, { id: is_set[0].id });
    await interaction.reply({ ephemeral: true, content: `Stopped following ${module}` });
  }
}

module.exports = {
  data: command,

  // its named execute to standardise it across modules
  async execute(db: PG_Manager, interaction: CommandInteraction) {
    // split out by the subcommand
    switch (interaction.options.getSubcommand()) {
      case "new" : {
        return await assignment_new(interaction, db);
      }
      case "list" : {
        return await assignment_list(interaction, db);
      }
      case "edit" : {
        return await assignment_edit(interaction, db);
      }
      case "delete" : {
        return await assignment_delete(interaction, db);
      }
      case "semester" : {
        return await assignment_semester(interaction, db);
      }
      default: {
        await interaction.reply('Please select a subcommand');
      }
    }
  },


  async reaction(db: PG_Manager, reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser) {
    // check if its an assignment message
    const assignments = await db.get_items(Assignments, {
      server: reaction.message.guildId,
      message_id: reaction.message.id,
    });

    if (assignments.length === 0) {
      // not an assignment
      return;
    }

    // if it is check teh database
    const reacts = await db.get_items(AssignmentsReact, { user: user.id, assignment: assignments[0].id });

    // if it is in teh db then remove
    if (reacts.length > 0) {
      await db.update_one(AssignmentsReact, { id: reacts[0].id, complete: false });
    } else {
      // if it is not then add
      await db.update_one(AssignmentsReact, {
        assignment: assignments[0].id,
        user: user.id,
        complete: true,
        notified: false,
      });
    }
  },

  async buttons(db: PG_Manager, interaction: ButtonInteraction) {
    const split = interaction.customId.split("_");

    if (split.length < 3) {
      return;
    }

    switch (split[1]) {
      case "semester" : {
        return await assignment_semester_module(interaction, db, split[2]);
      }
      default: {
        return;
      }
    }
  },
};

function array_to_object<T>(array: T[], field: string): {[propName: string]: boolean} {
  const result = {};

  for (const item of array) {
    if (typeof item[field] !== "undefined") {
      result[item[field]] = true;
    }
  }

  return result;
}
